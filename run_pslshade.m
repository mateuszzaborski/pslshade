function run_pslshade(Runs,fhd,C,problem_size,funcs,max_nfes,pop_size,optimum)

Rand_Seeds=load('input_data/Rand_Seeds.txt');

Alg_Name=[ 'psLSHADE_(' num2str(C(1)) num2str(C(2)) num2str(C(3)) ')'];

log_R2 = false;
log_tau = false;
log_perf = false;
log_perf2 = false;
log_volume = false;

fprintf('Running %s algorithm on D= %d\n',Alg_Name, problem_size)

for n=0:15
    RecordFEsFactor(n+1) = round(problem_size^((n/5)-3)*max_nfes);
end

progress = numel(RecordFEsFactor);
val_2_reach = 10^(-8);

for func_no = funcs
    fprintf('\n-------------------------------------------------------\n')
    fprintf('Function = %d, Dimension size = %d\n', func_no, problem_size)
    allerrorvals = zeros(progress, Runs);
    R2_vals = [];
    tau_vals = [];
    perf_vals = [];
    perf2_vals = [];
    volume_vals = [];
    %     you can use parfor if you have MATLAB Parallel Computing Toolbox
    for run_id = 1 : Runs
        rand_ind = mod(problem_size*func_no*Runs+run_id-Runs,length(Rand_Seeds));
        run_seed=Rand_Seeds(max(1, rand_ind));
        rng(run_seed,'twister');
        Run_RecordFEsFactor=RecordFEsFactor;
        run_funcvals = [];
        
        %%%%%%% LQ-R-SHADE %%%%%%%%
        %%  Parameter settings for L-SHADE
        D = problem_size;
      
        max_region = 100;
        min_region = -100;
        lower_bounds = repelem(min_region, D);
        upper_bounds = repelem(max_region, D);
        lu = [min_region * ones(1, D); max_region * ones(1, D)];
              
        p_best_rate = 0.11;
        arc_rate = 1.4;
        memory_size = 5;
        initial_sf = 0.5;
        initial_cr = 0.5;
        
        % Population decreasing
        max_pop_size = 18 * D;
        min_pop_size = 4.0;
        NP = max_pop_size;
        
        % LQ Archive params
        df_quad = 2*D+1;
        df_full = 2*D + D*(D-1)/2 + 1;
        df_full_plus = 2*D + D*(D-1)/2 + 2*D+ 1;      
        
        % Meta-model parameters
        Nsurr = 5;
        use_model = false;
        use_model_plus = true;
        
        %% Algorithm start
        nfes = 0;
        %%%%% CEC fitness storing
        bsf_fit_var = 1e+30;
        %%%%% CEC fitness storing
        R2_vals_run = [];
        tau_vals_run = [];
        perf_vals_run = [];
        perf2_vals_run = [];
        volume_vals_run = [];
        
        if use_model == true
           NQ = 2*df_full; 
        end
        if use_model_plus == true
           NQ = 2*df_full_plus; 
        end
        if use_model == false && use_model_plus == false
           NQ = 0; 
        end
        
        
        %% LQ Archive
        q = NaN(NQ,D);
        f_q = Inf(NQ,1);
        n_M = 0;
        
        %% Initial population - normal
        %pop = lower_bounds + rand(NP, D) .* (upper_bounds - lower_bounds);
        pop = double((upper_bounds - lower_bounds).*lhsdesign(single(NP),single(D)) + lower_bounds);
        fitness = NaN(NP, 1);
        for i = 1:NP
            %fitness(i) = cocoEvaluateFunction(problem, pop(i,:));
            fitness(i) = feval(fhd, pop(i,:)', func_no, C);
            nfes = nfes + 1;
            
            %%%%% CEC fitness storing start
            if fitness(i) < bsf_fit_var
                bsf_fit_var = fitness(i);
            end
            if(numel(Run_RecordFEsFactor) > 0 && nfes>=Run_RecordFEsFactor(1))
                run_funcvals = [run_funcvals; bsf_fit_var];
                Run_RecordFEsFactor(1)=[];
            end
            %%%%% CEC fitness storing end
            
            %% Update queue - conditional
            if NQ > 0
                if (fitness(i) < f_q(NQ)) && (~ismembertol(pop(i,:), q, 1e-12, 'ByRows',true)) && (~ismembertol(fitness(i), f_q, 1e-12, 'ByRows',true))
                    % Shift Queue
                    q(2:end,:) = q(1:end-1,:);
                    f_q(2:end) = f_q(1:end-1);
                    % Increment queue elements
                    n_M = min(n_M + 1, NQ);
                    % Add to queue
                    q(1,:) = pop(i,:);
                    f_q(1) = fitness(i);
                end
                %% Sort queue
                [f_q_sorted, f_q_sorted_idx] = sort(f_q);
                f_q = f_q_sorted;
                q = q(f_q_sorted_idx,:);
            end
        end
        
        memory_sf = initial_sf .* ones(memory_size, 1);
        memory_cr = initial_cr.* ones(memory_size, 1);
        memory_pos = 1;
        
        archive.NP = round(arc_rate * NP); % the maximum size of the archive
        archive.pop = zeros(0, D); % the solutions stored in te archive
        archive.funvalues = zeros(0, 1); % the function value of the archived solutions
        
        %% main loop
        while nfes < max_nfes
            nfes_start = nfes;
            %% Memory
            [temp_fit, sorted_index] = sort(fitness, 'ascend');
            
            mem_rand_index = ceil(memory_size * rand(NP, 1));
            mu_sf = memory_sf(mem_rand_index);
            mu_cr = memory_cr(mem_rand_index);
            
            %% for generating crossover rate
            cr = normrnd(mu_cr, 0.1);
            term_pos = find(mu_cr == -1);
            cr(term_pos) = 0;
            cr = min(cr, 1);
            cr = max(cr, 0);
            
            %% Mutation
            % for generating scaling factor
            sf = mu_sf + 0.1 * tan(pi * (rand(NP, Nsurr) - 0.5));
            
            for i = 1:Nsurr
                pos = find(sf(:,i) <= 0);
                while ~ isempty(pos)
                    sf(pos,i) = mu_sf(pos) + 0.1 * tan(pi * (rand(length(pos), 1) - 0.5));
                    pos = find(sf(:,i) <= 0);
                end
            end
            
            sf = min(sf, 1);
            r0 = [1 : NP];
            popAll = [pop; archive.pop];
            
            vi = NaN(NP, D, Nsurr);
            for i = 1:Nsurr
                [r1, r2] = gnR1R2mod(NP, size(popAll, 1), r0);
                
                pNP = max(single(round(p_best_rate * NP)), 2); %% choose at least two best solutions
                randindex = ceil(rand(1, NP) .* pNP); %% select from [1, 2, 3, ..., pNP]
                randindex = max(1, randindex); %% to avoid the problem that rand = 0 and thus ceil(rand) = 0
                pbest = pop(sorted_index(randindex), :); %% randomly choose one of the top 100p% solutions
                
                %%
                sf_i = sf(:,i);
                vi(:,:, i) = pop + sf_i(:, ones(1, D)) .* (pbest - pop + pop(r1, :) - popAll(r2, :));
                vi(:,:, i) = boundConstraint(vi(:,:, i), pop, lu);
            end
            
            %% Crossover
            mask = rand(NP, D) > cr(:, ones(1, D)); % mask is used to indicate which elements of ui comes from the parent
            rows = (1 : NP)';
            %cols = floor(rand(pop_size, 1) * single(problem_size))+1; % choose one position where the element of ui doesn't come from the parent
            cols =  randi([1 D],NP,1); % choose one position where the element of ui doesn't come from the parent
            jrand = sub2ind([NP D], rows, cols);
            mask(jrand) = false;
            ui = vi;
            for i = 1:Nsurr
                ui_i = ui(:,:,i);
                ui_i(mask) = pop(mask);
                ui(:,:, i) = ui_i;
            end
            
            %% Meta-model   
            %% Full model
            if n_M >= df_full && use_model == true
                %% Build model - full
                X = [NaN(n_M, df_full-1), ones(n_M,1)];
                X(:, 1:df_quad-1) = [q(1:n_M,:) .* q(1:n_M, :), q(1:n_M, :)];
                col_idx = df_quad;
                for i = 1:D
                    for j = (i+1):D
                        X(:, col_idx) = q(1:n_M,i) .* q(1:n_M,j);
                        col_idx = col_idx + 1;
                    end
                end
                Y = f_q(1:n_M);
                X2 = X'*X;
                b = X2\X'*Y;
                %% R2 calculation
                Yhat = b'*X';
                SStot = sum((Y-mean(Y)) .* (Y-mean(Y)));
                SSres = sum((Y - Yhat') .* (Y - Yhat'));
                R2 = 1 - SSres ./ SStot;
                %% Designate current surrogate value
                M_ui = NaN(NP, Nsurr);
                for i = 1:Nsurr
                    for j = 1:NP
                        X_surr = [NaN(1, df_full-1), ones(1,1)];
                        X_surr(1:df_quad-1) = [ui(j,:,i) .* ui(j,:,i), ui(j,:,i)];
                        col_idx = df_quad;
                        for k = 1:D
                            for m = (k+1):D
                                X_surr(col_idx) = ui(j,k,i) .* ui(j,m,i);
                                col_idx = col_idx + 1;
                            end
                        end
                        M_ui(j,i) = sum(b .* X_surr');
                    end
                end
            elseif n_M >= df_full_plus && use_model_plus == true
                %% Build model - full plus
                X = [NaN(n_M, df_full_plus-1), ones(n_M,1)];
                X(:, 1:df_quad-1) = [q(1:n_M,:) .* q(1:n_M, :), q(1:n_M, :)];
                col_idx = df_quad;
                for i = 1:D
                    for j = (i+1):D
                        X(:, col_idx) = q(1:n_M,i) .* q(1:n_M,j);
                        col_idx = col_idx + 1;
                    end
                end
                X(:, col_idx:(col_idx+D-1)) = 1./q(1:n_M, :);
                X(:, (col_idx+D):(col_idx+2*D-1)) = 1./(q(1:n_M, :) .* q(1:n_M, :));
                
                Y = f_q(1:n_M);
                X2 = X'*X;
                b = X2\X'*Y;
                %% R2 calculation
                Yhat = b'*X';
                SStot = sum((Y-mean(Y)) .* (Y-mean(Y)));
                SSres = sum((Y - Yhat') .* (Y - Yhat'));
                R2 = 1 - SSres ./ SStot;
                %% Designate current surrogate value
                M_ui = NaN(NP, Nsurr);
                for i = 1:Nsurr
                    for j = 1:NP
                        X_surr = [NaN(1, df_full_plus-1), ones(1,1)];
                        X_surr(1:df_quad-1) = [ui(j,:,i) .* ui(j,:,i), ui(j,:,i)];
                        col_idx = df_quad;
                        for k = 1:D
                            for m = (k+1):D
                                X_surr(col_idx) = ui(j,k,i) .* ui(j,m,i);
                                col_idx = col_idx + 1;
                            end
                        end
                        X_surr(:, col_idx:(col_idx+D-1)) = 1./ui(j,:,i);
                        X_surr(:, (col_idx+D):(col_idx+2*D-1)) = 1./(ui(j,:,i) .* ui(j,:,i));
                        M_ui(j,i) = sum(b .* X_surr');
                    end
                end
            else
                ui =  ui(:,:, 1);
            end
            
            
            %% Choose best ui using surrogate M-value
            if  use_model == true || use_model_plus == true
                [best_surr, best_surr_idx] = min(M_ui,[],2);
                ui_best = NaN(NP, D);
                for i = 1:NP
                    ui_best(i,:) = ui(i,:, best_surr_idx(i));
                end
                
                %% Meta-model selection performance
                if log_perf == true
                    M_ui_real = NaN(NP, Nsurr);
                    for j = 1:NP 
                        M_ui_real(j,:) = feval(fhd, squeeze(ui(j,:,:)), func_no, C);
                    end  
                    [best_real, best_real_idx] = min(M_ui_real,[],2);
                    share_perf = sum(best_surr_idx == best_real_idx) / NP;
                    perf_vals_run = [perf_vals_run, nfes, share_perf];
                end
                if log_perf2 == true
                    funvals_first = M_ui_real(:,1);
                    funvals_surr = funvals_first;
                    for j = 1:NP
                        funvals_surr(j) = M_ui_real(j, best_surr_idx(j));
                    end                
                    if(C(1)==1)
                        funvals_surr = funvals_surr-optimum(func_no);
                        funvals_first = funvals_first-optimum(func_no);
                    end
                    perf2_vals_run = [perf2_vals_run, nfes, mean(funvals_surr), mean(funvals_first)];
                end              
                
                ui = ui_best;
                % Final sf
                sf_copy = NaN(NP);
                for i = 1:NP
                    sf_copy(i) = sf(i, best_surr_idx(i));
                end
                sf = sf_copy;
                % Surrogate fitness
                surr_fitness = best_surr;
            else
                ui_best = NaN(NP, D);
                for i = 1:NP
                    ui_best(i,:) = ui(i,:, 1);
                end
                ui = ui_best;
                % Final sf
                sf = sf(:,1);
            end
                        
            %% Offspring evaluation
            children_fitness = zeros(NP, 1);
            for i = 1:size(ui,1)
                %children_fitness(i) = cocoEvaluateFunction(problem, ui(i,:));
                children_fitness(i) = feval(fhd, ui(i,:)', func_no, C);
                nfes = nfes + 1;
                %%%%% CEC fitness storing start
                if children_fitness(i) < bsf_fit_var
                    bsf_fit_var = children_fitness(i);
                end
                if(numel(Run_RecordFEsFactor) > 0 && nfes>=Run_RecordFEsFactor(1))
                    run_funcvals = [run_funcvals; bsf_fit_var];
                    Run_RecordFEsFactor(1)=[];
                end
                %%%%% CEC fitness storing end
                if NQ > 0
                    if n_M < NQ
                        %% Update queue
                        % Shift Queue
                        q(2:end,:) = q(1:end-1,:);
                        f_q(2:end) = f_q(1:end-1);
                        % Increment queue elements
                        n_M = min(n_M + 1, NQ);
                        % Add to queue
                        q(1,:) = ui(i,:);
                        f_q(1) = children_fitness(i);
                    else
                        %% Update queue - conditional
                        if (children_fitness(i) < f_q(NQ)) && (~ismembertol(ui(i,:), q, 1e-12, 'ByRows',true)) && (~ismembertol(children_fitness(i), f_q, 1e-12, 'ByRows',true))
                            q(NQ,:) = ui(i,:);
                            f_q(NQ) = children_fitness(i);
                        end
                    end
                    %% Sort queue
                    [f_q_sorted, f_q_sorted_idx] = sort(f_q);
                    f_q = f_q_sorted;
                    q = q(f_q_sorted_idx,:);
                end
            end
            
            %% Model tau-kendall
            if use_model == true || use_model_plus == true
                tau = corr(children_fitness, surr_fitness,'type','Kendall');
            end
            
            if log_R2 == true
                R2_vals_run = [R2_vals_run, nfes, R2];
            end
            if log_tau == true
                tau_vals_run = [tau_vals_run, nfes, tau];
            end
            if log_volume == true
                volume = prod(max(pop) - min(pop));
                volume_vals_run = [volume_vals_run, nfes, volume]; 
            end
            
            %% Selection
            dif = abs(fitness - children_fitness);
            I = (fitness > children_fitness);
            goodCR = cr(I == true);
            goodF = sf(I == true);
            dif_val = dif(I == true);
            
            archive = updateArchive(archive, pop(I == true, :), fitness(I == true));
            pop(I == true, :) = ui(I == true, :);
            fitness = min([fitness, children_fitness],[], 2);
            
            
            %% Update SF and CR memory
            num_success_params = numel(goodCR);
            
            if num_success_params > 0
                sum_dif = sum(dif_val);
                dif_val = dif_val / sum_dif;
                
                %% for updating the memory of scaling factor
                memory_sf(memory_pos) = (dif_val' * (goodF .^ 2)) / (dif_val' * goodF);
                
                %% for updating the memory of crossover rate
                if max(goodCR) == 0 || memory_cr(memory_pos)  == -1
                    memory_cr(memory_pos)  = -1;
                else
                    memory_cr(memory_pos) = (dif_val' * (goodCR .^ 2)) / (dif_val' * goodCR);
                end
                
                memory_pos = memory_pos + 1;
                if memory_pos > memory_size;  memory_pos = 1; end
            end
            
            %% for resizing the population size
            plan_pop_size = round((((min_pop_size - max_pop_size) / max_nfes) * nfes) + max_pop_size);
            
            if NP > plan_pop_size
                reduction_ind_num = NP - plan_pop_size;
                if NP - reduction_ind_num <  min_pop_size
                    reduction_ind_num = NP - min_pop_size;
                end
                
                NP = NP - reduction_ind_num;
                
                for r = 1 : reduction_ind_num
                    [valBest, indBest] = sort(fitness, 'ascend');
                    worst_ind = indBest(end);
                    pop(worst_ind,:) = [];
                    fitness(worst_ind,:) = [];
                end
                
                archive.NP = round(arc_rate * NP);
                
                if size(archive.pop, 1) > archive.NP
                    rndpos = randperm(size(archive.pop, 1));
                    rndpos = rndpos(1 : archive.NP);
                    archive.pop = archive.pop(rndpos, :);
                end
            end         
        end
        
        if(C(1)==1)
            run_funcvals=run_funcvals-optimum(func_no);
        end
        
        run_funcvals(run_funcvals<val_2_reach)=0;
        
        fprintf('%d th run, best-so-far error value = %1.8e\n', run_id , run_funcvals(end))
        allerrorvals(:, run_id) = run_funcvals;
        
        if log_R2 == true
            R2_vals(:, run_id) = R2_vals_run;
        end
        if log_tau == true
            tau_vals(:, run_id) = tau_vals_run;
        end
        if log_perf == true
            perf_vals(:, run_id) = perf_vals_run;
        end
        if log_perf2 == true
            perf2_vals(:, run_id) = perf2_vals_run;
        end
        if log_volume == true
            volume_vals(:, run_id) = volume_vals_run;
        end        
    end %% end 1 run
    
    [~, sorted_index] = sort(allerrorvals(end,:), 'ascend');
    allerrorvals = allerrorvals(:, sorted_index);
    
    fprintf('min_funvals:\t%e\n',min(allerrorvals(end,:)));
    fprintf('median_funvals:\t%e\n',median(allerrorvals(end,:)));
    fprintf('mean_funvals:\t%e\n',mean(allerrorvals(end,:)));
    fprintf('max_funvals:\t%e\n',max(allerrorvals(end,:)));
    fprintf('std_funvals:\t%e\n',std(allerrorvals(end,:)));
    
    file_name=sprintf('Results/%s_%s_%s.txt',Alg_Name,int2str(func_no),int2str(problem_size));
    save(file_name, 'allerrorvals', '-ascii');
    
    if log_R2 == true
        file_name_R2=sprintf('R2/%s_%s_%s.csv',Alg_Name,int2str(func_no),int2str(problem_size));
        csvwrite(file_name_R2,R2_vals);
    end
    if log_tau == true
        file_name_tau=sprintf('tau/%s_%s_%s.csv',Alg_Name,int2str(func_no),int2str(problem_size));
        csvwrite(file_name_tau,tau_vals);
    end
    if log_perf == true
        file_name_perf=sprintf('perf/%s_%s_%s.csv',Alg_Name,int2str(func_no),int2str(problem_size));
        csvwrite(file_name_perf,perf_vals);
    end
    if log_perf2 == true
        file_name_perf=sprintf('perf2/%s_%s_%s.csv',Alg_Name,int2str(func_no),int2str(problem_size));
        csvwrite(file_name_perf,perf2_vals);
    end
    if log_volume == true
        file_name_perf=sprintf('volume/%s_%s_%s.csv',Alg_Name,int2str(func_no),int2str(problem_size));
        csvwrite(file_name_perf,volume_vals);
    end
    
    
end %% end 1 function run

end
